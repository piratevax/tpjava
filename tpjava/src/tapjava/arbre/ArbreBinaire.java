package tapjava.arbre;

public interface ArbreBinaire<T> {
	
	/**
	 * Retourne le noeud racine de l'arbre
	 * @return
	 */
	T contenu();
	
	ArbreBinaire<T> sousArbreGauche();
	
	ArbreBinaire<T> sousArbreDroit();
	
	boolean isLeaf();
	
}
