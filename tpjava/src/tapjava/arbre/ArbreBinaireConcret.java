package tapjava.arbre;

public class ArbreBinaireConcret<T> implements ArbreBinaire<T> {
	private T contenu;
	private ArbreBinaire droit;
	private ArbreBinaire gauche;

	public ArbreBinaireConcret(T contenu) {
		this.contenu = contenu;
		droit = null;
		gauche = null;
	}

	public ArbreBinaireConcret(T contenu, ArbreBinaire<T> sousArbreGauche, ArbreBinaire<T> sousArbreDroit) {
		this(contenu);
		gauche = sousArbreGauche;
		droit = sousArbreDroit;
	}
	
	@Override
	public T contenu() {
		// TODO Auto-generated method stub
		return contenu;
	}

	@Override
	public ArbreBinaire<T> sousArbreGauche() {
		return gauche;
	}

	@Override
	public ArbreBinaire<T> sousArbreDroit() {
		return droit;
	}

	@Override
	public boolean isLeaf() {
		if (droit == null && gauche == null)
			return true;
		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
