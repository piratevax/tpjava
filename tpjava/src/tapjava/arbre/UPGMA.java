package tapjava.arbre;

import java.util.ArrayList;
import java.util.List;

public class UPGMA {

	private static List<ArbreBinaire<Integer>> foretInitiale(int nombre) {
		List<ArbreBinaire<Integer>> foret = new ArrayList<ArbreBinaire<Integer>>();
		
		for (int i = 0; i < nombre; i++) {
			ArbreBinaire<Integer> arbre = new ArbreBinaireConcret<Integer>(i);
			foret.add(arbre);
		}
		
		return foret;
	}

	private static void reduire(List<ArbreBinaire<Integer>> foret, int [][] score, int k) {
//		int iMax, jMax;
//		for (int i = 0; i < k; i++) {
//			for (int j = i+1; j < k; j++) {
//				if (score[i][j] > score[iMax][jMax]) {
//					iMax = i;
//					jMax = j;
//				}
//			}
//		}
//		foret.set(k, new ArbreBinaireConcret<Integer>(k, foret.get(iMax), foret.get(jMax)));
//		for (int i = 0; i < k; i++) {
//			
//		}
		int n = foret.size();
		int sMax = Integer.MIN_VALUE;
		ArbreBinaire<Integer> aiMax = null;
		ArbreBinaire<Integer> ajMax = null;
		for (int i = 0; i < n; i++) {
			ArbreBinaire<Integer> ai = foret.get(i);
			int iIdx = ai.contenu();
			for (int j = i+1; j < n; j++) {
				ArbreBinaire<Integer> aj = foret.get(j);
				int jIdx = aj.contenu();
				int s = score[iIdx][jIdx];
				if (s > sMax) {
					sMax = s;
					aiMax = ai;
					ajMax = aj;
				}
			}
		}
		int i = aiMax.contenu();
		int j = ajMax.contenu();
		foret.remove(aiMax);
		foret.remove(ajMax);		
		for (ArbreBinaire<Integer> al:foret) {
			int l = al.contenu();
			score[k][l] = (score[i][l] + score[j][j]) / 2;
			score[l][k] = score[k][l]; 
		}

		ArbreBinaire<Integer> ak = new ArbreBinaireConcret<Integer>(k, aiMax, ajMax);
		foret.add(ak);

	}
	
	public static ArbreBinaire<Integer> arbre(int [][] score) {
		int nombre = score.length;
		int [][] score2 = new int [nombre*2][nombre*2];
		for (int i = 0; i < nombre; i++ )
			for (int j = 0; j < nombre; j++)
				score2[i][j] = score[i][j];
		List<ArbreBinaire<Integer>> foret = foretInitiale(nombre);
		
		for (int k = nombre; foret.size() > 1; k++) {
			reduire(foret, score2, k);
		}
		
		return foret.get(0);
	}
	
	private static void afficher(ArbreBinaire<Integer> arbre) {
		System.out.println(arbre.contenu());
		if (!arbre.isLeaf()) {
			afficher(arbre.sousArbreGauche());
			afficher(arbre.sousArbreDroit());
		}
	}
	
	public static void main(String[] args) {
		int [][] score = {{0, 3, 2}, {3, 0, 15}, {2, 15, 0}};
		afficher(arbre(score));
	}
}