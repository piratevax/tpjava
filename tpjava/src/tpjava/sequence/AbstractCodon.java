package tpjava.sequence;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractCodon implements Codon {

	private static final String CODONS = "tttttcttattgtcttcctcatcgtattactaatagtgttgctgatgg" + 
			"cttctcctactgcctcccccaccgcatcaccaacagcgtcgccgacgg" + 
			"attatcataatgactaccacaacgaataacaaaaagagtagcagaagg" + 
			"gttgtcgtagtggctgccgcagcggatgacgaagagggtggcggaggg";
	
	private Map<String, Character> map;
	
	public char getAcideAmine(String codon) {
		codon = codon.toLowerCase().replace('u', 't');;
		return map.get(codon);
	}

	void setMap(String acidesAmines) {
		map = new HashMap<String, Character>();
		int n = CODONS.length();
		for (int i = 0; i < n; i += 3) {
			String codon = CODONS.substring(i,  i+3);
			char aa = acidesAmines.charAt(i/3);
			map.put(codon, aa);
		}
	}
	
}
