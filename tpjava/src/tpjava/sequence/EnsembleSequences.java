/**
 * 
 */
package tpjava.sequence;

import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * @author xaxa
 *
 */
public interface EnsembleSequences extends Iterable<Sequence> {

	/**
	 * retourne le nombre de séquences de l’ensemble.
	 * @return un entier
	 */
	int getNombre();
	
	/**
	 * retourne la i-ème séquence de l’ensemble.
	 * @param i ème séquence
	 * @return Sequence
	 */
	Sequence get(int i);
	
	/**
	 * qui retourne la séquence de l’ensemble dont l’identificateur est donné.
	 * @param idf identificateur d'un ensemble de séquence
	 * @return Sequence
	 */
	Sequence get(String idf);
	
	/**
	 * ajoute une séquence à l’ensemble.
	 * @param sequence
	 */
	void add(Sequence sequence);
	
	/**
	 * lit plusieurs séquences au format fasta
	 * @param reader
	 * @return boolean
	 */
	boolean read(BufferedReader reader);
	
	boolean read(BufferedReader reader, Sequence sequence) throws InvalidSequenceException;
	
	/**
	 * écrit plusieurs séquences au format fata
	 * @param writer
	 */
	void write(PrintWriter writer);
}
