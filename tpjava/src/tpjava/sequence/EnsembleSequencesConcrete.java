/**
 * 
 */
package tpjava.sequence;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

/**
 * @author xaxa
 *
 */
public class EnsembleSequencesConcrete implements EnsembleSequences {

	List<Sequence> list;
	Map<String, Sequence> map;
	
	/**
	 * 
	 */
	public EnsembleSequencesConcrete() {
		super();
		list = new ArrayList<Sequence>();
		map = new HashMap<String, Sequence>();
	}

	
	@Override
	public Iterator<Sequence> iterator() {
		return list.iterator();
	}

	@Override
	public int getNombre() {
		return list.size();
	}

	@Override
	public Sequence get(int i) {
		return list.get(i);
	}
	
	public Sequence getSlow(String idf) {
		for (Sequence sequence : this) {
			String id = sequence.getId();
			if (id.equals(idf))
				return sequence;
		}
		return null;
	}
	
	public Sequence get(String idf) {
		return map.get(idf);
	}

	@Override
	public void add(Sequence sequence) {
		list.add(sequence);
		map.put(sequence.getId(), sequence);
	}

	@Override
	public boolean read(BufferedReader reader) {
		try {
			Sequence sequence = new SequenceConcrete();
			while (sequence.read(reader)) {
				Sequence seq = new SequenceConcrete(sequence.getId(), sequence.getOrganism(), sequence.getSequence());
				add(seq);
			}
		} catch (InvalidSequenceException e) {
			return false;
		}
		return (getNombre() > 0);
	}

	@Override
	public void write(PrintWriter writer) {
		for (Sequence sequence : this) {
			sequence.write(writer);
			writer.println();
		}
	}


	@Override
	public boolean read(BufferedReader reader, Sequence sequence) throws InvalidSequenceException {
//		Sequence sequenceCopy = sequence.getSameTypeSequence("","","");
////				sequence.getId(),
////				sequence.getOrganism(),
////				sequence.getSequence());
//		while (sequenceCopy.read(reader)) {
//			Sequence seq = sequenceCopy.getSameTypeSequence(
//					sequenceCopy.getId(),
//					sequenceCopy.getOrganism(),
//					sequenceCopy.getSequence());
////			System.out.println(sequenceCopy.getId());
//			add(seq);
//		}
		Sequence seq = new SequenceConcrete();
		while (seq.read(reader)) {
			add(sequence.getSameTypeSequence(seq.getId(), seq.getOrganism(),
					seq.getSequence()));
		}
		return (getNombre() > 0);
	}

	public static void main(String args[]) {
//		Sequence seq1 = new SequenceConcrete("id1", "", "atcgtat");
//		Sequence seq2 = new SequenceConcrete("id2", "", "atcgtatatcgtat");
//		Sequence seq3 = new SequenceConcrete("id3", "", "atcgtatatcgtatatcgtat");
//		sequences.add(seq1); sequences.add(seq2); sequences.add(seq3);
//		
//		System.out.println(sequences.get(2).getSequence());
//		System.out.println();
//		System.out.println(sequences.get("id1").getSequence());
//		try {
//			EnsembleSequences sequences = new EnsembleSequencesConcrete();
//			BufferedReader reader;
//			reader = new BufferedReader(new FileReader("ps00023.fasta"));
//			sequences.read(reader, new SequenceConcrete());
////			PrintWriter writer = new PrintWriter(new FileWriter("adns2.fa"));
////			sequences.write(writer);
//			reader.close();
//			// C-x(2)-P-F-x-[FYWIV]-x(7)-C-x(8,10)-W-C-x(4)-[DNSR]-[FYW]- x(3,5)-[FYW]-x-[FYWI]-C
//			String motif = "C..PF.[FYWIV].{7}C.{8,10}WC.{4}[DNSR][FYW].{3,5}[FYW].[FYWI]C";
//			for (Sequence sequence : sequences) {
//				String occ = sequence.occurence(motif);
//				int pos = sequence.getSequence().indexOf(occ);
//				System.out.printf("%s (%d-%d): %s\n", sequence.getId(), pos, pos+occ.length()-1, occ);
//			}
////			writer.close();
		try {
			String currentType = new String("ADN");
			Sequence type;// = (String) typeComboBox.getSelectedItem());
			if (currentType.equalsIgnoreCase("Protéine")) {
				type = new ProteineConcrete();
			} else if (currentType.equalsIgnoreCase("ADN")) {
				type = new SequenceADNConcrete();
			} else {
				type = new SequenceARNConcrete();
			}
			BufferedReader reader = new BufferedReader(new FileReader("adns.fa"));
			EnsembleSequences sequences = new EnsembleSequencesConcrete();
			sequences.read(reader, type);
			reader.close();
			System.out.print(sequences.getNombre()+"\n");
			for (Sequence sequence:sequences) {
				System.out.print(sequence.getId()+"\n");
			}
			System.out.print("\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
