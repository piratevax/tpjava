/**
 * 
 */
package tpjava.sequence;

/**
 * @author xaxa
 *
 */
public class InvalidSequenceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public InvalidSequenceException() {
		super();
	}

	/**
	 * @param message
	 */
	public InvalidSequenceException(String message) {
		super(message);
	}
}
