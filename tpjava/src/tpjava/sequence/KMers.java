/**
 * 
 */
package tpjava.sequence;

import java.util.Set;

/**
 * @author xaxa
 *
 */
public interface KMers {
	
	int k();
	Set<String> kMers();
	Set<String> communs(KMers kMers);
	int nombreCommuns(KMers kMers);

}
