/**
 * 
 */
package tpjava.sequence;

import java.util.HashSet;
import java.util.Set;

/**
 * @author xaxa
 *
 */
public class KMersConcrets implements KMers {
	private int k;
	private Set<String> kMers;

	/**
	 * 
	 */
	public KMersConcrets(int k) {
		this.k = k;
		kMers = new HashSet<String>();
	}
	

	public KMersConcrets(int k, String chaine) {
		this(k);
		int l = chaine.length() - k;
		for (int i = 0; i <= l; i++) {
			String kMer = chaine.substring(i, i+k);
			kMers.add(kMer);
		}
	}
	
	public KMersConcrets(int k, Sequence sequence) {
		this(k, sequence.getSequence());
	}

	@Override
	public int k() {
		return k;
	}
	
	@Override
	public Set<String> kMers() {
		return new HashSet<String>(kMers);
	}

	@Override
	public Set<String> communs(KMers kMers) {
		Set<String> set = kMers.kMers();
		Set<String> communs = new HashSet<String>();
		
		for (String kMer:kMers()) {
			if (set.contains(kMer)) {
				communs.add(kMer);
			}
		}
		return communs;
	}


	public Set<String> communs2(KMers kMers) {
		Set<String> set = kMers.kMers();
		
		set.retainAll(kMers());
		return set;
	}
	
	@Override
	public int nombreCommuns(KMers kMers) {
		return communs2(kMers).size();
	}
	
	public static void main(String[] args) {
		Sequence seq1 = new SequenceConcrete("","","ACTCTCG");
		Sequence seq2 = new SequenceConcrete("","","ACTACTCG");
		KMers kMers1 = new KMersConcrets(3, seq1);
		KMers kMers2 = new KMersConcrets(3, seq2);
		System.out.println(kMers1.nombreCommuns(kMers2));
	}
	
}
