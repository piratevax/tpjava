package tpjava.sequence;

import java.util.List;

public interface Proteine extends Sequence {

	List<Occurrence> find(SequenceADN sequence, int k, int delta, int scoreMin);
	
}
