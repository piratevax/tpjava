package tpjava.sequence;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

public class ProteineConcrete extends SequenceConcrete implements Proteine {

	public static final String ALPHABET = "ARNDCEQGHILKMFPSTWYVUOarndceqghilkmfpstwyvuo*-";
	
	public ProteineConcrete() {
		super();
	}

	public ProteineConcrete(String id, String organism, String sequence) throws InvalidSequenceException {
		super(id, organism, sequence);
		if (!isValid(sequence, ALPHABET)) {
			throw new InvalidSequenceException("Invalid sequence exception");
		}
	}
	
	@Override
	public boolean read(BufferedReader reader) throws InvalidSequenceException {
		return validateRead(reader, ALPHABET);
	}

	@Override
	public List<Occurrence> find(SequenceADN sequence, int k, int delta, int scoreMin) {
		List<Occurrence> occs = new ArrayList<Occurrence>();
		Proteine proteine = sequence.traduction();
		proteine.find(sequence, k, delta, scoreMin);
		
		return occs;
	}
	
	@Override
	public Sequence getSameTypeSequence(String id, String organism, String sequence) throws InvalidSequenceException {
		// TODO Auto-generated method stub
		return new ProteineConcrete(id, organism, sequence);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
