package tpjava.sequence;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.regex.Matcher;

/**
 * 
 * @author xaxa
 *
 */
public interface Sequence {
	
	/**
	 * Retourne l'identifieur de la séquence.
	 * @return String
	 */
	String getId();
	
	/**
	 * Retourne l'organisme auquel appartient la séquence.
	 * @return String
	 */
	String getOrganism();
	
	/**
	 * Retourne la séquence.
	 * @return String
	 */
	String getSequence();
	
	/**
	 * Retourne la taille de la séquence.
	 * @return int
	 */
	int length();
	
	/**
	 * Retourne le caractère de la séquence à la position <b>pos</b>.
	 * (avec <b>1 inf pos length()</b>) 
	 * @param pos
	 * @return char
	 */
	char get(int pos);
	
	/**
	 * Lit une séquence au format <i>fasta</i>, cette méthode retournera <b>True</b> si et seulement si une séquence au format <i>fasta</i> a pu être lu dans le <b>reader</b>.
	 * @param reader
	 * @return boolean
	 */
	boolean read(BufferedReader reader) throws InvalidSequenceException;
	
	/**
	 * Écrit une séquence au format <i>fasta</i>.
	 * @param writer
	 */
	void write(PrintWriter writer);
	
	Sequence getSameTypeSequence ( String id, String organism, String sequence) throws InvalidSequenceException;
	
	Matcher matcher(String motif);
	
	boolean contains (String motif);
	
	String occurence(String motif);
	
	String[] occurences(String motif);
	
	String repetition();
	
	String repetition(int minSize);
	
	Occurrence findFirst(Sequence sequence, int k, int delta, int scoreMin);
	
	List<Occurrence> find(Sequence sequence, int k, int delta, int scoreMin);
}
