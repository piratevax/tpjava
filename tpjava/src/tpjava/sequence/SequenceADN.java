package tpjava.sequence;

/**
 * 
 * @author xaxa
 *
 */
public interface SequenceADN extends SequenceNucleique {

	/**
	 * Retourne un objet <b>SequenceARN</b> contenant la séquence d'ARN transcrite.
	 * @return
	 */
	SequenceARN transcript();
}
