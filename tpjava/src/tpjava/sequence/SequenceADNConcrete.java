package tpjava.sequence;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author xaxa
 *
 */
public class SequenceADNConcrete extends SequenceNucleiqueConcrete implements SequenceADN {

	public static final String ALPHABET = "ATCGNatcgn-";
	
	public SequenceADNConcrete() {
		super();
	}
	
	public SequenceADNConcrete(String id, String organism, String sequence) throws InvalidSequenceException {
		super(id, organism, sequence);
		if (! isValid(sequence, ALPHABET))
			throw new InvalidSequenceException("Invalid seqeuence exception!");
	}
	
	@Override
	public SequenceARN transcript() {
		String arn = convert(getSequence(), ALPHABET, SequenceARNConcrete.ALPHABET);
		try {
			return new SequenceARNConcrete(getId(), getOrganism(), arn);
		}
		catch (InvalidSequenceException e) {
			return null;
		}
	}
	
	@Override
	public boolean read(BufferedReader reader) throws InvalidSequenceException {
		return validateRead(reader, ALPHABET);
	}
	
	@Override
	public Sequence getSameTypeSequence(String id, String organism, String sequence) throws InvalidSequenceException {
		// TODO Auto-generated method stub
		return new SequenceADNConcrete(id, organism, sequence);
	}
	
	@Override
	public Proteine traduction(boolean mitochondrial) {
		return transcript().traduction(mitochondrial);
	}
	
	List<Occurrence> find(Proteine sequence, int k, int delta, int scoreMin) {
		List<Occurrence> occs = new ArrayList<Occurrence>();
		Proteine seq = traduction();
		seq.find(sequence, k, delta, scoreMin);
		
		return occs;
	}
	
	public static void main(String []args) {
		try {
			BufferedReader in = new BufferedReader(new FileReader("adn.fasta"));
			SequenceADN adn = new SequenceADNConcrete();
			adn.read(in);
			in.close();
			SequenceARN arn = adn.transcript();
			PrintWriter out = new PrintWriter(new FileWriter("arn.fa"));
			arn.write(out);
			out.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
