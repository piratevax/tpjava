package tpjava.sequence;

/**
 * 
 * @author xaxa
 *
 */
public interface SequenceARN extends SequenceNucleique {

	/**
	 * Retourne un bojet <b>SequenceADN</b> contenant la séquence à l'orignie de la transcription
	 * @return
	 */
	SequenceADN transcriptInv();
}
