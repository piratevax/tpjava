package tpjava.sequence;

import java.io.BufferedReader;
import java.io.PrintWriter;


/**
 * 
 * @author xaxa
 *
 */
public class SequenceARNConcrete extends SequenceNucleiqueConcrete implements SequenceARN {

	public static final String ALPHABET = "AUCGNaucgn-";
	
	public SequenceARNConcrete() {
		super();
	}
	
	public SequenceARNConcrete(String id, String organism, String sequence) throws InvalidSequenceException {
		super(id, organism, sequence);
		if (! isValid(sequence, ALPHABET))
			throw new InvalidSequenceException("Invalid seqeuence exception!");
	}

	@Override
	public SequenceADN transcriptInv() {
		String adn = convert(getSequence(), ALPHABET, SequenceADNConcrete.ALPHABET);
		try {
			return new SequenceADNConcrete(getId(), getOrganism(), adn);
		}
		catch (InvalidSequenceException e) {
			return null;
		}
	}
	
	@Override
	public boolean read(BufferedReader reader) throws InvalidSequenceException {
		return validateRead(reader, ALPHABET);
	}
	
	@Override
	public Sequence getSameTypeSequence(String id, String organism, String sequence) throws InvalidSequenceException {
		// TODO Auto-generated method stub
		return new SequenceARNConcrete(id, organism, sequence);
	}

	@Override
	public Proteine traduction(boolean mitochondrial) {
		Codon codon = null;
		if(mitochondrial) {
			codon = new MitochondrialCodon();
		}
		else {
			codon = new UniversalCodon();
		}
		String sequence = getSequence();
		int n = sequence.length();
		StringBuffer proteine = new StringBuffer();
		for (int i = 0; i < n; i += 3) {
			if (i+3 <= n) {
				String c = sequence.substring(i,  i+3);
				char aa = codon.getAcideAmine(c);
				proteine.append(aa);
			}
		}
		try {
			return new ProteineConcrete(getId(), getOrganism(), proteine.toString());
		} catch (InvalidSequenceException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String []args) {
		try {
//			BufferedReader in = new BufferedReader(new FileReader("arn.fa"));
			SequenceARN arn = new SequenceARNConcrete("id", "organism", "AGCCUGCCAAGCAAACUUCACUGGAGUGUGCGUAGCAUGCUAGUAACUGCAUCUGAAUCU");
//			arn.read(in);
//			in.close();
			SequenceADN adn = arn.transcriptInv();
			PrintWriter out = new PrintWriter(System.out);
			adn.write(out);
			out.flush();
//			PrintWriter out = new PrintWriter(new FileWriter("inv.fa"));
//			adn.write(out);
//			out.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
