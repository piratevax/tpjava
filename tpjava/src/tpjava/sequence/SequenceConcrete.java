package tpjava.sequence;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author xaxa
 *
 */
public class SequenceConcrete implements Sequence {
	private String id;
	private String organism;
	private String sequence;
	
	private static final  int LINE_SIZE = 60;
	
	/**
	 * Création d'une séquence vide
	 */
	public SequenceConcrete() {
//		this.id = "";
//		this.organism = "";
//		this.sequence = "";
	}
	
	/**
	 * Création d'une séquence avec ses attributs
	 * @param id
	 * @param organism
	 * @param Sequence
	 */
	public SequenceConcrete(String id, String organism, String sequence) {
		this();
		this.id = id;
		this.organism = organism;
		this.sequence = sequence;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getOrganism() {
		return organism;
	}

	@Override
	public String getSequence() {
		return sequence;
	}

	@Override
	public int length() {
		try {
			return sequence.length();
		}
		catch (Exception e) {
			return 0;
		}
	}

	@Override
	public char get(int pos) {
		return sequence.charAt(pos-1);
	}

	
	protected boolean rawRead(BufferedReader reader) {
		// >gi|373251181|ref|NG_001742.2| Mus musculus olfactory receptor GA_x5J8B7W2GLP-600-794 (LOC257854) pseudogène on chromosome 2
		try {
			String line = reader.readLine();
			String [] tab = line.split("\\|");
			this.id = tab[3];
			this.organism = tab[4];
//			System.out.println("***id:"+id+"; org: "+organism);
			StringBuilder seq = new StringBuilder();
			while ((line = reader.readLine()) != null && line.trim().length() > 0) {
				seq.append(line.trim());
			}
			this.sequence = seq.toString();
			return true;
		}
		catch (Exception e) {
			//e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean read(BufferedReader reader) throws InvalidSequenceException {
		return rawRead(reader);
	}

	@Override
	public void write(PrintWriter writer) {
		String line = ">|||"+getId()+"|"+getOrganism();
		writer.println(line);
		String seq = getSequence();
		int n = length();
		for (int i = 0; i < n; i += LINE_SIZE) {
			line = seq.substring(i, Math.min(n, i+LINE_SIZE));
			writer.println(line);
		}
	}
	
	protected static boolean isValid(String sequence, String alphabet) {
		int length = sequence.length();
		for (int i = 0; i < length; i++) {
			char c = sequence.charAt(i);
			if (alphabet.indexOf(c) < 0)
				return false;
		}
		return true;
	}
	
	protected boolean validateRead(BufferedReader reader, String alphabet) throws InvalidSequenceException {
		if (rawRead(reader))
			return false;
		if (! isValid(getSequence(), alphabet))
			throw new InvalidSequenceException("Invalid sequence exception!");
		return true;
	}
	
	@Override
	public Sequence getSameTypeSequence(String id, String organism, String sequence) throws InvalidSequenceException {
		// TODO Auto-generated method stub
		return new SequenceConcrete(id, organism, sequence);
	}

	@Override
	public Matcher matcher(String motif) {
		Pattern p = Pattern.compile(motif.toUpperCase());
		Matcher m = p.matcher(getSequence().toUpperCase());
		return m;
	}
	
	@Override
	public boolean contains (String motif) {
		Matcher m = matcher(motif);
		return m.find();
	}
	
	@Override
	public String occurence(String motif) {
		Matcher m = matcher(motif);
		if (m.find()) {
			String occ = getSequence().substring(m.start(), m.end());
			return occ;
		}
		return null;
	}
	
	@Override
	public String[] occurences(String motif) {
		List<String> occs = new ArrayList<String> ();
		Matcher m = matcher(motif);
		while (m.find()) {
			String occ = getSequence().substring(m.start(), m.end());
			occs.add(occ);
		}
		return occs.toArray(new String[0]);
	}
	
	@Override
	public String repetition() {
		Matcher m = matcher("(.{3,}).*\\1");
		if (m.find())
			return m.group(1);
		return null;
	}

	@Override
	public String repetition(int minSize) {
		Matcher m = matcher("(.{"+minSize+",}).*\\1");
		if (m.find())
			return m.group(1);
		return null;
	}
	
	@Override
	public Occurrence findFirst(Sequence sequence, int k, int delta, int scoreMin) {
		int lenReq = sequence.length();
		int lenTar = this.sequence.length();
		String req = sequence.getSequence();
		String tar = this.getSequence();
		String sub = new String();
//		for (int i = 0; i < lenTar; i += delta) {
		for (int i = 0; i < lenTar; i++) {
			for (int j = -delta; j <= delta && (lenReq+j) < lenTar; j++) {
				/*if (tar.regionMatches(true, i, req, 0, lenReq-j-1)) {*/
					KMers kMersSub = new KMersConcrets(k, tar.substring(i, lenReq-j-1));
					KMers kMersReq = new KMersConcrets(k, sequence);
					if (kMersReq.nombreCommuns(kMersSub) >= scoreMin) {
						return new Occurrence(i, lenReq-j-1);
					}
				/*}*/
				
			}
		}
		
		return null;
	}

	@Override
	public List<Occurrence> find(Sequence sequence, int k, int delta, int scoreMin) {
		List<Occurrence> occurrences = new ArrayList<Occurrence>();
		/*Sequence tmp = sequence;
		int n = sequence.length(), i = 0;
		while ((i + delta) < n ) {
			occurrences.add(findFirst(tmp, k, delta, scoreMin));
		}*/
		int lenReq = sequence.length();
		int lenTar = this.sequence.length();
		String req = sequence.getSequence();
		String tar = this.getSequence();
		String sub = new String();
//		for (int i = 0; i < lenTar; i += delta) {
		for (int i = 0; i < lenTar; i += delta) {
			for (int j = -delta; j <= delta && (lenReq+j) < lenTar; j++) {
				/*if (tar.regionMatches(true, i, req, 0, lenReq-j-1)) {*/
					KMers kMersSub = new KMersConcrets(k, tar.substring(i, lenReq-j-1));
					KMers kMersReq = new KMersConcrets(k, sequence);
					if (kMersReq.nombreCommuns(kMersSub) >= scoreMin) {
						occurrences.add(new Occurrence(i, lenReq-j-1));
					}
				/*}*/
				
			}
		}
		return occurrences;
	}

	public static void main(String args[]) {
		/*try {
			BufferedReader in = new BufferedReader(new FileReader("ps00023.fasta"));
			Sequence seq = new SequenceConcrete();
			if (!seq.read(in))
				System.err.println("Read sequence problem!");
			in.close();
			//System.out.println(seq.getId()+"\n"+seq.getSequence());
			
			// C-x(2)-P-F-x-[FYWIV]-x(7)-C-x(8,10)-W-C-x(4)-[DNSR]-[FYW]- x(3,5)-[FYW]-x-[FYWI]-C
			String motif = "C..PF.[FYWIV].{7}C.{8,10}WC.{4}[DNSR][FYW].{3,5}[FYW].[FYWI]C";
			System.out.println(seq.occurence(motif));
			
//			String[] occs = seq.occurences("ACT.");
//			System.out.println(Arrays.toString(occs));
//			System.out.println(seq.repetition(10));
			
			 
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		Sequence seq1 = new SequenceConcrete("","","ACTCT");//TCG");
		Sequence seq2 = new SequenceConcrete("","","ACTACTCG");
		Occurrence occ = seq2.findFirst(seq1, 3, 2, 1);
		if (occ != null) {
			System.out.println("("+occ.start+":"+occ.end+") "+seq2.getSequence().substring(occ.start, occ.end+1));
		}
	}

}
