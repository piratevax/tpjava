package tpjava.sequence;

/**
 * 
 * @author xaxa
 *
 */
public interface SequenceNucleique extends Sequence {
	
	Proteine traduction();
	
	Proteine traduction(boolean mitochondrial);

}
