package tpjava.sequence;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * 
 * @author xaxa
 *
 */
public class SequenceNucleiqueConcrete extends SequenceConcrete implements SequenceNucleique {
	
	public static final String ALPHABET = "ATUCGNatucgn-";
	
	public SequenceNucleiqueConcrete() {
		super();
	}
	
	public SequenceNucleiqueConcrete(String id, String organism, String sequence) throws InvalidSequenceException {
		super(id, organism, sequence);
		if (! isValid(sequence, ALPHABET))
			throw new InvalidSequenceException("Invalid seqeuence exception!");
	}
	
	@Override
	public boolean read(BufferedReader reader) throws InvalidSequenceException {
		return validateRead(reader, ALPHABET);
	}
	
	protected static String convert(String sequence, String alpha1, String alpha2) {
		StringBuilder seqConvert = new StringBuilder();
		int n = sequence.length();
		
		for (int i = 0; i < n; i++) {
			char c = sequence.charAt(i);
			int pos = alpha1.indexOf(c);
			c = alpha2.charAt(pos);
			seqConvert.append(c);
		}

		return seqConvert.toString();
	}
	
	@Override
	public Sequence getSameTypeSequence(String id, String organism, String sequence) throws InvalidSequenceException {
		return new SequenceNucleiqueConcrete(id, organism, sequence);
	}

	@Override
	public Proteine traduction() {
		return traduction(false);
	}

	@Override
	public Proteine traduction(boolean mitochondrial) {
		try {
			String sequence = getSequence().toLowerCase();
			if (sequence.indexOf('t') >= 0) {
				SequenceADN adn = new SequenceADNConcrete(getId(), getOrganism(), getSequence());
				return adn.traduction(mitochondrial);
			}
			else {
				SequenceARN arn = new SequenceARNConcrete(getId(), getOrganism(), getSequence());
				return arn.traduction(mitochondrial);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;	
		}
	}
	
	public static void main(String []args) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader("adn.fasta"));
			SequenceNucleique sequence = new SequenceNucleiqueConcrete();
			sequence.read(reader);
			System.out.println(sequence.getSequence());
			Proteine proteine = sequence.traduction();
			System.out.println(proteine.getSequence());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
