/**
 * 
 */
package tpjava.sequence.alignement;

import tpjava.sequence.EnsembleSequences;
import tpjava.sequence.Sequence;

/**
 * @author xaxa
 *
 */
public interface Alignement extends EnsembleSequences {
	int length();
	
	Sequence getSansGaps(int i);
	
	Sequence getSansGaps(String idf);
	
	char [] getColonne(int k);
	
	public Alignement projection(int numSeq1, int numSeq2);
	
	public int scoreSP(Score score);
}
