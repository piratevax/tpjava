package tpjava.sequence.alignement;

import java.util.Arrays;

import tapjava.arbre.ArbreBinaire;
import tapjava.arbre.UPGMA;
import tpjava.sequence.EnsembleSequences;
import tpjava.sequence.InvalidSequenceException;
import tpjava.sequence.KMers;
import tpjava.sequence.KMersConcrets;
import tpjava.sequence.Sequence;
import tpjava.sequence.SequenceADNConcrete;

public class AlignementAvecGapAffine extends AlignementConcret {

	public AlignementAvecGapAffine() {
		super();
	}
	
	public AlignementAvecGapAffine(Sequence s1, Sequence s2, ScoreGap score) {//, boolean local) {
		this();
//		super(s1, s2, score);
		int [][][] matrix = matricesScore(s1, s2, score, false);
		int S = 0, E = 1, F = 2;
		int m = s1.length();
		int n = s2.length();
		int go = score.poidsOuverture();
		int ge = score.poidsExtension();
//		for (int x = 0; x <= m; x++)
//			System.out.println(Arrays.toString(matrix[S][x]));
//		System.out.println("score: "+matrix[S][m][n]);
		int i = m;
		int j = n;
		int [][] etat = matrix[S];
		StringBuilder sa1 = new StringBuilder();
		StringBuilder sa2 = new StringBuilder();
		char empty = '-';
		
		while (i > 0 && j > 0) {
			char ai = s1.get(i);
			char bj = s2.get(j);
			if (etat == matrix[S]) {
				int rempl = matrix[S][i-1][j-1] + score.poids(ai,  bj);
				if (matrix[S][i][j] == rempl) {
					sa1.append(ai);
					sa2.append(bj);
					i--; j--;
				} else if (matrix[S][i][j] == matrix[E][i][j]) {
					etat = matrix[E];
				} else {
					etat = matrix[F];
				}
			} else if (etat == matrix[E]) {
				sa1.append(ai);
				sa2.append(empty);
				if (matrix[E][i][j] == matrix[S][i-1][j] - go)  {
					etat = matrix[S];
				}
				i--;
			} else {
				sa1.append(empty);
				sa2.append(bj);
				if (matrix[F][i][j] == matrix[S][i-1][j] - go)  {
					etat = matrix[S];
				}
				j--;
			}
		}
		while (i > 0) {
			char ai = s1.get(i);
			sa1.append(ai);
			sa2.append(empty);
			i--;
		}
		while (i > 0) {
			char bj = s2.get(j);
			sa1.append(bj);
			sa2.append(empty);
			j--;
		}
		sa1.reverse();
		sa2.reverse();
		try {
			Sequence seq = s1.getSameTypeSequence(s1.getId(), s1.getOrganism(), sa1.toString());
			add(seq);
			seq = s2.getSameTypeSequence(s2.getId(), s2.getOrganism(), sa2.toString());
			add(seq);
		} catch (InvalidSequenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public AlignementAvecGapAffine(Alignement a1, Alignement a2, ScoreGapMultiple score) {
		this();
//		super(s1, s2, score);
		int [][][] matrix = matricesScore(a1, a2, score, false);
		int S = 0, E = 1, F = 2;
		int m = a1.length();
		int n = a2.length();
		int go = score.poidsOuverture();
		int ge = score.poidsExtension();
//		for (int x = 0; x <= m; x++)
//			System.out.println(Arrays.toString(matrix[S][x]));
//		System.out.println("score: "+matrix[S][m][n]);
		int i = m;
		int j = n;
		int [][] etat = matrix[S];
		char empty = '-';
		int N1 = a1.getNombre();
		int N2 = a2.getNombre();
		int N = N1 + N2;
		StringBuilder [] sa = new StringBuilder[N];
		for (int x = 0; x < N; x++) {
			sa[x] = new StringBuilder();
		}
		while (i > 0 && j > 0) {
			char [] ai = a1.getColonne(i);
			char [] bj = a2.getColonne(j);	
			char [] colonne = fusionColonne(ai, bj);
			if (etat == matrix[S]) {
				int rempl = matrix[S][i-1][j-1] + score.poids(colonne);
				if (matrix[S][i][j] == rempl) {
					for (int x = 0; x < N; x++) {
						sa[x].append(colonne[x]);
					}
					i--; j--;
				} else if (matrix[S][i][j] == matrix[E][i][j]) {
					etat = matrix[E];
				} else {
					etat = matrix[F];
				}
			} else if (etat == matrix[E]) {
				for (int x = 0; x < N1; x++) {
					sa[x].append(ai[x]);
				}
				for (int x = 0; x < N2; x++) {
					sa[x+N1].append(empty);
				}
				if (matrix[E][i][j] == matrix[S][i-1][j] - go)  {
					etat = matrix[S];
				}
				i--;
			} else {
				for (int x = 0; x < N1; x++) {
					sa[x].append(empty);
				}
				for (int x = 0; x < N2; x++) {
					sa[x+N1].append(bj[x]);
				}
				if (matrix[F][i][j] == matrix[S][i-1][j] - go)  {
					etat = matrix[S];
				}
				j--;
			}
		}
		while (i > 0) {
			char []ai = a1.getColonne(i);
			for (int x = 0; x < N1; x++) {
				sa[x].append(ai[x]);
			}
			for (int x = 0; x < N-N1; x++) {
				sa[x+N1].append(empty);
			}
			i--;
		}
		while (i > 0) {
			char [] bj = a2.getColonne(j);
			for (int x = 0; x < N-N2; x++) {
				sa[x].append(empty);
			}
			for (int x = 0; x < N2; x++) {
				sa[x+N-N2].append(bj[x]);
			}
			j--;
		}
		for (int x = 0; x < N; x++) {
			sa[x].reverse();
		}
		try {
			for (int x = 0; x < N1; x++) {
				Sequence s = a1.get(x);
				Sequence seq = s.getSameTypeSequence(s.getId(), s.getOrganism(), sa[x].toString());
				add(seq);
			}
			for (int x = 0; x < N2; x++) {
				Sequence s = a2.get(x);
				Sequence seq = s.getSameTypeSequence(s.getId(), s.getOrganism(), sa[x+N1].toString());
				add(seq);
			}
		} catch (InvalidSequenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public AlignementAvecGapAffine(EnsembleSequences sequences, ScoreGapMultiple scoreMultiple, ScoreGap scorePaires) {
		this();
		ArbreBinaire<Integer> arbre = upgma(sequences, scorePaires);
		Alignement align = alignement(sequences, arbre, scoreMultiple);
		
		for (Sequence seq:sequences) {
			seq = align.get(seq.getId());
			add(seq);
		}
	}
	
	public AlignementAvecGapAffine(EnsembleSequences sequences, ScoreGapMultiple scoreMultiple, int k) {
		this();
		int [][] scorePaires = scoreKmersPaires(sequences, k);
		ArbreBinaire<Integer> arbre = UPGMA.arbre(scorePaires);
		Alignement align = alignement(sequences, arbre, scoreMultiple);
        for (Sequence seq : align) {
            add(seq);
        }

		
	}
	
	protected static int [][][] matricesScore(Sequence s1, Sequence s2, ScoreGap score, boolean local) {
		int matrixNumber = 3;
		int S = 0, E = 1, F = 2;
//		int [][] S = matrix[0];
//		int [][] E = matrix[1];
//		int [][] F = matrix[2];
		int m = s1.length();
		int n = s2.length();
		int go = score.poidsOuverture();
		int ge = score.poidsExtension();
		int[][][] matrix = new int[matrixNumber][m+1][n+1];
		
		matrix[S][0][0] = 0;
		matrix[E][0][0] = -go + ge;
		matrix[F][0][0] = -go + ge;
		for (int i = 1; i <= m; i++) {
			matrix[F][i][0] = Integer.MIN_VALUE/2;
			matrix[E][i][0] = matrix[E][i-1][0] - ge;
			matrix[S][i][0] = matrix[E][i][0];
		}
		for (int j = 1; j <= n; j++) {
			matrix[E][0][j] = Integer.MIN_VALUE/2;
			matrix[F][0][j] = matrix[F][0][j-1] - ge;
			matrix[S][0][j] = matrix[F][0][j];
		}
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				matrix[E][i][j] = Math.max(matrix[S][i-1][j] - go, matrix[E][i][j-1] - go);
				matrix[F][i][j] = Math.max(matrix[S][i][j-1] - go, matrix[F][i-1][j] - go);
//				matrix[E][i][j] = Math.max(matrix[E][i-1][j] - go, matrix[S][i][j-1] - go);
//				matrix[F][i][j] = Math.max(matrix[F][i][j-1] - go, matrix[S][i-1][j] - go);
				int remp  = matrix[S][i-1][j-1] + score.poids(s1.get(i), s2.get(j));
				matrix[S][i][j] = Math.max(Math.max(remp, matrix[E][i][j]), matrix[F][i][j]);
				if (local)
					matrix[S][i][j] = Math.max(0, matrix[S][i][j]);
			}
		}
		
		return matrix;
	}

	protected static int [][][] matricesScore(Alignement a1, Alignement a2, ScoreGapMultiple score, boolean local) {
		int matrixNumber = 3;
		int S = 0, E = 1, F = 2;
//		int [][] S = matrix[0];
//		int [][] E = matrix[1];
//		int [][] F = matrix[2];
		int m = a1.length();
		int n = a2.length();
		int go = score.poidsOuverture();
		int ge = score.poidsExtension();
		int[][][] matrix = new int[matrixNumber][m+1][n+1];
		
		matrix[S][0][0] = 0;
		matrix[E][0][0] = -go + ge;
		matrix[F][0][0] = -go + ge;
		
		for (int i = 1; i <= m; i++) {
			matrix[F][i][0] = Integer.MIN_VALUE;
			matrix[E][i][0] = matrix[E][i-1][0] - ge;
			matrix[S][i][0] = matrix[E][i][0];
		}
		for (int j = 1; j <= n; j++) {
			matrix[E][0][j] = Integer.MIN_VALUE/2;
			matrix[F][0][j] = matrix[F][0][j-1] - ge;
			matrix[S][0][j] = matrix[F][0][j];
		}
		for (int i = 1; i <= m; i++) {
			char [] ai = a1.getColonne(i);
			for (int j = 1; j <= n; j++) {
				char [] bj = a2.getColonne(j);
				char [] colonne = fusionColonne(ai,bj);
				matrix[E][i][j] = Math.max(matrix[S][i-1][j] - go, matrix[E][i][j-1] - go);
				matrix[F][i][j] = Math.max(matrix[S][i][j-1] - go, matrix[F][i-1][j] - go);
//				matrix[E][i][j] = Math.max(matrix[E][i-1][j] - go, matrix[S][i][j-1] - go);
//				matrix[F][i][j] = Math.max(matrix[F][i][j-1] - go, matrix[S][i-1][j] - go);
				int remp  = matrix[S][i-1][j-1] + score.poids(colonne);
				matrix[S][i][j] = Math.max(Math.max(remp, matrix[E][i][j]), matrix[F][i][j]);
				if (local)
					matrix[S][i][j] = Math.max(0, matrix[S][i][j]);
			}
		}
		return matrix;
	}

	private static char [] fusionColonne(char [] col1, char [] col2) {
		char [] col = new char[col1.length +col2.length];

		for (int i = 0; i < col1.length; i++) {
			col[i] = col1[i];
		}
		for (int i = 0; i < col2.length; i++) {
			col[i+col1.length] = col2[i];
		}
		return col;
	}
	
	private static int [][] scorePaires(EnsembleSequences sequences, ScoreGap score) {
		int n = sequences.getNombre();
		int [][] scoreP = new int[n][n];
		
		for (int i = 0; i < n; i++) {
				Sequence s1 = sequences.get(i);
			for (int j = i+1; j < n; j++) {
				Sequence s2 = sequences.get(j);
				int [][][] matrix = matricesScore(s1, s2, score, false);
				scoreP [i][j] = scoreP[j][i] = matrix[0][s1.length()][s2.length()];
			}
		}
		
		return scoreP;
	}
	
	private static ArbreBinaire<Integer> upgma(EnsembleSequences sequences, ScoreGap score) {
		int [][] sp = scorePaires(sequences, score);
		
		return UPGMA.arbre(sp);
	}
	
	private static Alignement alignement(EnsembleSequences sequences, ArbreBinaire<Integer> arbre, ScoreGapMultiple score) {
		
		if (arbre.isLeaf()) {
			Alignement a = new AlignementAvecGapAffine();
			Sequence sequence = sequences.get(arbre.contenu());
			a.add(sequence);
			return a;
		}
		
		Alignement a1 = alignement(sequences, arbre.sousArbreGauche(), score);
		Alignement a2 = alignement(sequences, arbre.sousArbreDroit(), score);
		Alignement a = new AlignementAvecGapAffine(a1, a2, score);
		return a;
	}
	
	private static int[][] scoreKmersPaires(EnsembleSequences sequences, int k) {
		int n = sequences.getNombre();
		int [][] scoreKP = new int[k][k];
		
		for (int i = 0; i < n; i++) {
			KMers kS1 = new KMersConcrets(k, sequences.get(i));
			for (int j = i+1; j < n; j++) {
				KMers kS2 = new KMersConcrets(k, sequences.get(j));
				scoreKP[i][j] = scoreKP[j][i] = kS1.nombreCommuns(kS2);; 
			}
		}
		
		return scoreKP;
	}
	
	static private int scorePaireSequences(Alignement alig2, ScoreGap score) {
		Sequence s1 = alig2.get(0);
		Sequence s2 = alig2.get(1);
		int [][][] matrix = matricesScore(s1, s2, score, false);
		
		return matrix[0][s1.length()][s2.length()];
	}
	
	public int scoreSP(ScoreGap score) {
		int scoreSP = 0;
		int n = getNombre();
		
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				scoreSP += scorePaireSequences(new AlignementConcret(get(i), get(j), score), score);
			}
		}
		return scoreSP;
	}
	
	public static void main(String[] args) {
		try {
			ScoreGap score = new ScoreSimpleGap(20, 3);
			Sequence s1 = new SequenceADNConcrete("s1", "", "ACTCAGGAGA");
			Sequence s2 = new SequenceADNConcrete("s2", "", "ACGGAGCGA");
			Alignement align1 = new AlignementAvecGapAffine(s1, s2, score);
			System.out.println();
			for (int i = 0; i < align1.getNombre(); i++) {
				System.out.println(align1.get(i).getSequence());
			}
			
			Sequence s3 = new SequenceADNConcrete("s3", "", "CTCAGGAGA");
			Sequence s4 = new SequenceADNConcrete("s4", "", "ACGGAGCG");
			Alignement align2 = new AlignementAvecGapAffine(s3, s4, score);
			for (int i = 0; i < align2.getNombre(); i++) {
				System.out.println(align2.get(i).getSequence());
			}
			
			ScoreGapMultiple scoreM = new ScoreGapMultipleSP(score);
			Alignement align = new AlignementAvecGapAffine(align1, align2, scoreM);
			for (int i = 0; i < align.getNombre(); i++) {
				System.out.println(align.get(i).getSequence());
			}

		} catch (InvalidSequenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
