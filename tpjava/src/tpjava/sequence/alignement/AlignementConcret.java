/**
 * 
 */
package tpjava.sequence.alignement;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;

import tpjava.sequence.EnsembleSequencesConcrete;
import tpjava.sequence.InvalidSequenceException;
import tpjava.sequence.Sequence;
import tpjava.sequence.SequenceADNConcrete;
import tpjava.sequence.alignement.Alignement;

/**
 * @author xaxa
 *
 */
public class AlignementConcret extends EnsembleSequencesConcrete implements Alignement {

	/**
	 * 
	 */
	public AlignementConcret() {
		super();
	}
	
	public AlignementConcret(Sequence s1, Sequence s2, Score score) {
		this();
		int [][] matrix = matriceScore(s1, s2, score);
		int i = s1.length();
		int j = s2.length();
		StringBuilder u = new StringBuilder();
		StringBuilder v = new StringBuilder();
		char empty = '-';
	
		while (i > 0 && j > 0) {
			int remp = matrix[i-1][j-1] + score.poids(s1.get(i), s2.get(j));
			int supp = matrix[i-1][j] + score.poids(s1.get(i), empty);
//			int ins = matrix[i][j-1] + score.poids(empty, s2.get(j));
			if (matrix[i][j] == remp) {
				u.append(s1.get(i));
				v.append(s2.get(j));
				i--; j--;
			} else if (matrix[i][j] == supp ) {
				u.append(s1.get(i));
				v.append(empty);
				i--;				
			} else {
				u.append(empty);
				v.append(s2.get(j));
				j--;
			}
		}
		u.reverse();
		v.reverse();
		try {
			Sequence sequence1 = s1.getSameTypeSequence(s1.getId(), s1.getOrganism(), u.toString());
			Sequence sequence2 = s2.getSameTypeSequence(s2.getId(), s2.getOrganism(), v.toString());
			add(sequence1);
			add(sequence2);
		} catch (InvalidSequenceException e) {
		}
	}
	
	/* (non-Javadoc)
	 * @see tpjava.sequence.alignment.Alignment#length()
	 */
	@Override
	public int length() {
		if(getNombre() == 0)
			return 0;
		return get(0).length();
	}

	/* (non-Javadoc)
	 * @see tpjava.sequence.alignment.Alignment#getSansGaps(int)
	 */
	@Override
	public Sequence getSansGaps(int i) {
		Sequence seq = get(i);
		String sequence = seq.getSequence().replaceAll("-", "");
		try {
			return seq.getSameTypeSequence(seq.getId(), seq.getOrganism(), sequence);
		} catch (InvalidSequenceException e) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see tpjava.sequence.alignment.Alignment#getSansGaps(java.lang.String)
	 */
	@Override
	public Sequence getSansGaps(String idf) {
		Sequence seq = get(idf);
		String sequence = seq.getSequence().replaceAll("-", "");
		try {
			return seq.getSameTypeSequence(seq.getId(), seq.getOrganism(), sequence);
		} catch (InvalidSequenceException e) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see tpjava.sequence.alignment.Alignment#getColonne(int)
	 */
	@Override
	public char[] getColonne(int k) {
		char [] colonne = new char[getNombre()];
		for (int i = 0; i < getNombre(); i++)
			colonne[i] = get(i).get(k);
		return colonne;
	}

	private static int[][] matriceScore(Sequence s1, Sequence s2, Score score) {
		int m = s1.length();
		int n = s2.length();
		int[][] matrix = new int[m+1][n+1];
		char empty = '-';
//		int i = s1.length();
//		int j = s2.length();
		
		matrix[0][0] = 0;
		for (int i = 1; i <= m; i++)
			matrix[i][0] = matrix[i-1][0] + score.poids(s1.get(i), empty);
		for (int j = 1; j <= n; j++)
			matrix[0][j] = matrix[0][j-1] + score.poids(empty, s2.get(j));
		
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				int temp = matrix[i-1][j-1] + score.poids(s1.get(i), s2.get(j));
				int supp = matrix[i-1][j] + score.poids(s1.get(i), empty);
				int ins = matrix[i][j-1] + score.poids(empty, s2.get(j));
				matrix[i][j] = Math.max(Math.max(temp, supp), ins);
			}
		}
		
		return matrix;
	}

	@Override
	public Alignement projection(int numSeq1, int numSeq2) {
		Sequence s1 = get(numSeq1);
		Sequence s2 = get(numSeq2);
		Score score = new ScoreSimple();
		Alignement align = new AlignementConcret(s1, s2, score);
		int n = s1.getSequence().length();
		int m = s2.getSequence().length();
		
		for (int i = n-1; i > -1; i--) {
			for (int j = n-1; j > -1; j--) {
				
			}
		}
		
		return align;
	}

	static private int scorePaireSequences(Alignement alig2, Score score) {
		Sequence s1 = alig2.get(0);
		Sequence s2 = alig2.get(1);
		int [][] matrixScore = matriceScore(s1, s2, score);
		
		return matrixScore[s1.length()][s2.length()];
	}
	
	public int scoreSP(Score score) {
		int scoreSP = 0;
		int n = getNombre();
		
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				scoreSP += scorePaireSequences(new AlignementConcret(get(i), get(j), score), score);
			}
		}
		
		return scoreSP;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
//			Sequence seq1 = new SequenceADNConcrete("u1", "", "CAT-GC");
//			Sequence seq2 = new SequenceADNConcrete("u2", "", "CA-TGC");
//			Sequence seq3 = new SequenceADNConcrete("u2", "", "CA-TCG");
//			Alignment align = new AlignmentConcret();
//			align.add(seq1);
//			align.add(seq2);
//			align.add(seq3);
//			System.out.printf("taille: %d\n", align.length());
//			System.out.printf("colonne 3: %s\n", Arrays.toString(align.getColonne(3)));
//			PrintWriter out = new PrintWriter(new FileWriter("align.fa"));
//			align.write(out);
//			out.close();
			

//			Sequence seq1 = new SequenceADNConcrete("u1", "", "CATGC");
//			Sequence seq2 = new SequenceADNConcrete("u2", "", "CATGC");
//			Score score = new ScoreSimple();
//			int[][] matrix = matriceScore(seq1, seq2, score);
//			for (int i = 0; i < seq1.length(); i++)
//				System.out.println(Arrays.toString(matrix[i]));
			
			Sequence seq1 = new SequenceADNConcrete("u1", "", "CATGC");
			Sequence seq2 = new SequenceADNConcrete("u2", "", "CATTGC");
			Score score = new ScoreSimple();
			Alignement align = new AlignementConcret(seq1, seq2, score);
			System.out.println(align.get(0).getSequence());
			System.out.println(align.get(1).getSequence());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
