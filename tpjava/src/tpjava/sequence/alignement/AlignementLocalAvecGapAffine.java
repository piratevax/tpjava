package tpjava.sequence.alignement;

import tpjava.sequence.InvalidSequenceException;
import tpjava.sequence.Sequence;

public class AlignementLocalAvecGapAffine extends AlignementAvecGapAffine {

	public AlignementLocalAvecGapAffine() {
		super();
	}

	public AlignementLocalAvecGapAffine(Sequence s1, Sequence s2, ScoreGap score) {
		int [][][] matrix = matricesScore(s1, s2, score, true);
		int S = 0, E = 1, F = 2;
		int m = s1.length();
		int n = s2.length();
		int go = score.poidsOuverture();
		int ge = score.poidsExtension();
		int maxS = matrix[S][m][n];
		int iMax = m;
		int jMax = n;
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				if (maxS < matrix[S][i][j]) {
					maxS = matrix[S][i][j];
					iMax = i;
					jMax = j;
				}
			}
		}
		int i = m;
		int j = n;
		int [][] etat = matrix[S];
		StringBuilder sa1 = new StringBuilder();
		StringBuilder sa2 = new StringBuilder();
		char empty = '-';
		
		while (i > 0 && j > 0 && matrix[S][i][j] > 0) {
			char ai = s1.get(i);
			char bj = s2.get(j);
			if (etat == matrix[S]) {
				int rempl = matrix[S][i-1][j-1] + score.poids(ai,  bj);
				if (matrix[S][i][j] == rempl) {
					sa1.append(ai);
					sa2.append(bj);
					i--; j--;
				} else if (matrix[S][i][j] == matrix[E][i][j]) {
					etat = matrix[E];
				} else {
					etat = matrix[F];
				}
			} else if (etat == matrix[E]) {
				sa1.append(ai);
				sa2.append(empty);
				if (matrix[E][i][j] == matrix[S][i-1][j] - go)  {
					etat = matrix[S];
				}
				i--;
			} else {
				sa1.append(empty);
				sa2.append(bj);
				if (matrix[F][i][j] == matrix[S][i-1][j] - go)  {
					etat = matrix[S];
				}
				j--;
			}
		}
		sa1.reverse();
		sa2.reverse();
		try {
			Sequence seq = s1.getSameTypeSequence(s1.getId(), s1.getOrganism(), sa1.toString());
			add(seq);
			seq = s2.getSameTypeSequence(s2.getId(), s2.getOrganism(), sa2.toString());
			add(seq);
		} catch (InvalidSequenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
