/**
 * 
 */
package tpjava.sequence.alignement;

/**
 * @author xaxa
 *
 */
public interface Score {
	
	int poids(char a, char b);

}
