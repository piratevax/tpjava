/**
 * 
 */
package tpjava.sequence.alignement;

/**
 * @author xaxa
 *
 */
public interface ScoreGap extends Score {
	
	int poidsOuverture();
	
	int poidsExtension();
}
