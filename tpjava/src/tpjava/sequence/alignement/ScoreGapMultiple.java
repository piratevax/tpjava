package tpjava.sequence.alignement;

public interface ScoreGapMultiple extends ScoreGap {
	
	/**
	 * Retourne le poids de substitution des caractères de la colonne d’un alignement multiple.
	 * @param colonne
	 * @return poids de substitution
	 */
	int poids(char []colonne);
	
}
