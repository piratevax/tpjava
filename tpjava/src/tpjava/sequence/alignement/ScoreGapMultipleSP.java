package tpjava.sequence.alignement;

public class ScoreGapMultipleSP implements ScoreGapMultiple {
	private ScoreGap score;

	
	public ScoreGapMultipleSP(ScoreGap score) {
		this.score = score;
	}
	
	@Override
	public int poidsOuverture() {
		return score.poidsOuverture();
	}

	@Override
	public int poidsExtension() {
		return score.poidsExtension();
	}

	@Override
	public int poids(char a, char b) {
		return score.poids(a, b);
	}

	@Override
	public int poids(char[] colonne) {
		int n = colonne.length;
		int poids = 0;
		char empty = '-';
//		int i = 0;
//		int j = n/2;
//		while (i < n/2 && j < n) {
//			char a = colonne[i], b = colonne[j];
//			if (a != empty && b != empty) {
//				poidsSubs = poids(a, b);
//				i++; j++;
//			} else {
//				int l = 0;
//			}
//		}
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				if (colonne[i] != empty || colonne[j] != empty)
					poids += score.poids(colonne[i], colonne[j]);
			}
		}
		
		return poids;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
