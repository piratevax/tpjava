/**
 * 
 */
package tpjava.sequence.alignement;

/**
 * @author xaxa
 *
 */
public class ScoreMatriciel implements Score {

	private int[][] matrix;
	private String alphabet;
	
	/**
	 * 
	 */
	public ScoreMatriciel(int[][] matrix, String alphabet) {
		this.matrix = matrix;
		this.alphabet = alphabet;
	}

	/* (non-Javadoc)
	 * @see tpjava.sequence.alignment.Score#poids(char, char)
	 */
	@Override
	public int poids(char a, char b) {
		int ia = alphabet.indexOf(Character.toUpperCase(a));
		int ib = alphabet.indexOf(Character.toUpperCase(b));
		
		if (ia >= 0 && ib >= 0) {
			return matrix[ia][ib];
		}
		return 0;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
