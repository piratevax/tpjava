package tpjava.sequence.alignement;

public class ScoreMatricielGap extends ScoreMatriciel implements ScoreGap {
	private int ouverture;
	private int extension;

	public ScoreMatricielGap(int[][] matrix, String alphabet, int ouverture, int extension) {
		super(matrix, alphabet);
		this.ouverture = ouverture;
		this.extension = extension;
	}

	@Override
	public int poidsOuverture() {
		return ouverture;
	}

	@Override
	public int poidsExtension() {
		return extension;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
