/**
 * 
 */
package tpjava.sequence.alignement;

/**
 * @author xaxa
 *
 */
public class ScoreSimpleGap extends ScoreSimple implements ScoreGap {
	private int ouverture;
	private int extension;

	/**
	 * 
	 */
	public ScoreSimpleGap(int ouverture, int extension) {
//		super();
		this.ouverture = ouverture;
		this.extension = extension;
	}

	/* (non-Javadoc)
	 * @see tpjava.sequence.alignment.ScoreGap#poidsOuverture()
	 */
	@Override
	public int poidsOuverture() {
		return ouverture;
	}

	/* (non-Javadoc)
	 * @see tpjava.sequence.alignment.ScoreGap#poidsExtension()
	 */
	@Override
	public int poidsExtension() {
		return extension;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
