package tpjava.sequence.fenetre;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tpjava.sequence.EnsembleSequences;
import tpjava.sequence.EnsembleSequencesConcrete;
import tpjava.sequence.InvalidSequenceException;
import tpjava.sequence.Occurrence;
import tpjava.sequence.Proteine;
import tpjava.sequence.ProteineConcrete;
import tpjava.sequence.Sequence;
import tpjava.sequence.SequenceADNConcrete;
import tpjava.sequence.SequenceARNConcrete;
import tpjava.sequence.alignement.Alignement;
import tpjava.sequence.alignement.AlignementAvecGapAffine;
import tpjava.sequence.alignement.AlignementConcret;
import tpjava.sequence.alignement.AlignementLocalAvecGapAffine;
import tpjava.sequence.alignement.Blosum62;
import tpjava.sequence.alignement.Score;
import tpjava.sequence.alignement.ScoreGap;
import tpjava.sequence.alignement.ScoreGapMultiple;
import tpjava.sequence.alignement.ScoreGapMultipleSP;
import tpjava.sequence.alignement.ScoreMatriciel;
import tpjava.sequence.alignement.ScoreMatricielGap;
import tpjava.sequence.alignement.ScoreSimple;
import tpjava.sequence.alignement.ScoreSimpleGap;

public class FenetreSequence extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextArea textArea;
	private JTextField textField;
	private JTextField textFieldMotif;
	private JTextField gapOpenField;
	private JTextField gapExtensionField;
	private JTextField seq1Field;
	private JTextField seq2Field;
	private JComboBox<String> typeComboBox;
	private final Dimension defaut = new Dimension(600, 600);
	private EnsembleSequences sequences;
	private String currentType;
	
	public FenetreSequence() {
		super("Fenêtre séquence");
		setLayout(new BorderLayout());
		setMinimumSize(defaut);
		setPreferredSize(defaut);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		textArea = new JTextArea();
//		textArea.setLineWrap(true);
		textArea.setAutoscrolls(true);
		textArea.setFont(new Font("monospaced", Font.PLAIN, 13));
		
		JScrollPane pane = new JScrollPane(textArea);
		
		JPanel panel = new JPanel(new FlowLayout());
		
		String []types = {"ADN", "ARN", "Protéine"};
		typeComboBox = new JComboBox<String>(types);
		typeComboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				currentType = new String();
				currentType = (String) typeComboBox.getSelectedItem();
			}
		});
		panel.add(typeComboBox);
		
		JButton ouvrir = new JButton("Ouvrir");
		ouvrir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ouvrir();
			}
		});
		panel.add(ouvrir);
		
		textField = new JTextField();
		Dimension textFieldDimension = new Dimension(120, 30);
		textField.setMinimumSize(textFieldDimension);
		textField.setPreferredSize(textFieldDimension);
		panel.add(textField);
		
		JButton afficher = new JButton("Afficher");
		afficher.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				afficher();
			}
		});
		panel.add(afficher);
		

		gapOpenField = new JTextField();
		gapOpenField.setPreferredSize(new Dimension(50, 30));
		panel.add(new JLabel("go"));
		panel.add(gapOpenField);
		gapExtensionField = new JTextField();
		gapExtensionField.setPreferredSize(new Dimension(50, 30));
		panel.add(new JLabel("ge"));
		panel.add(gapExtensionField);
		
		JButton alignerMultiple = new JButton("Aligner tout");
		alignerMultiple.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				alignerMultiple();
			}
		});
		panel.add(alignerMultiple);
		
		
		
		seq1Field = new JTextField();
		seq1Field.setPreferredSize(textFieldDimension);
		panel.add(new JLabel("id1"));
		panel.add(seq1Field);
		seq2Field = new JTextField();
		seq2Field.setPreferredSize(textFieldDimension);
		panel.add(new JLabel("id2"));
		panel.add(seq2Field);
		
		JButton aligner = new JButton("Aligner");
		aligner.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				aligner(false);
			}
		});
		panel.add(aligner);
		aligner = new JButton("Aligner localement");
		aligner.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				aligner(true);
			}
		});
		panel.add(aligner);
		
		textFieldMotif = new JTextField();
		textFieldMotif.setMinimumSize(textFieldDimension);
		textFieldMotif.setPreferredSize(textFieldDimension);
		panel.add(textFieldMotif);
		
		JButton recherche = new JButton("Rechercher");
		recherche.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				recherche();
			}
		});
		panel.add(recherche);
		
		JButton enregistrer = new JButton("Enregistrer");
		enregistrer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				enregistrer();
			}
		});
		panel.add(enregistrer);
		
		
		add(panel, BorderLayout.NORTH);
		add(pane, BorderLayout.CENTER);
		
		pack();
	}

	private void ouvrir() {
		JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
		int returnVal = chooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				currentType = new String();
				currentType = (String) typeComboBox.getSelectedItem();
				Sequence type;// = (String) typeComboBox.getSelectedItem());
				if (currentType.equalsIgnoreCase("Protéine")) {
					type = new ProteineConcrete();
				} else if (currentType.equalsIgnoreCase("ADN")) {
					type = new SequenceADNConcrete();
				} else {
					type = new SequenceARNConcrete();
				}
				BufferedReader reader = new BufferedReader(new FileReader(chooser.getSelectedFile()));
				sequences = new EnsembleSequencesConcrete();
				sequences.read(reader, type);
				reader.close();
				textArea.append(sequences.getNombre()+"\n");
				for (Sequence sequence:sequences) {
					textArea.append(sequence.getId()+"\n");
				}
				textArea.append("\n");
			} catch (InvalidSequenceException e) {
				JOptionPane.showMessageDialog(this, "Type de séquence invalide", "!!! ERREUR !!!", JOptionPane.ERROR_MESSAGE);
			} catch (Exception e) {
				//e.printStackTrace();
				JOptionPane.showMessageDialog(this, "File not found!", "!!! ERREUR !!!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private void afficher() {
		try {
			String id = textField.getText().trim();
			Sequence sequence = sequences.get(id);
			textArea.append(sequence.getId()+":\n"+sequence.getSequence()+"\n\n");
			textArea.setCaretPosition(textArea.getText().length()-1);;
		} catch (Exception e) {
			//e.printStackTrace();
			JOptionPane.showMessageDialog(this, "ID non trouvé!", "Info", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	private void recherche() {
		try {
			String motif = textFieldMotif.getText().trim();
			for (Sequence sequence:sequences) {
				String occ = sequence.occurence(motif);
				if (occ != null) {
					int position = sequence.getSequence().indexOf(occ);
					String id = sequence.getId();
					textArea.append(id+"("+position+"): "+occ+"\n");
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Motif non trouvé!", "Info", JOptionPane.INFORMATION_MESSAGE);
		}
		textArea.append("\n");
	}
	
	private void enregistrer() {
		JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
		int returnVal = chooser.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(chooser.getSelectedFile()));
				writer.println(textArea.getText());
				writer.close();
			} catch (Exception e) {
				//e.printStackTrace();
				JOptionPane.showMessageDialog(this, "File not found!", "!!! ERREUR !!!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private static int intField(JTextField field) {
		return Integer.parseInt(field.getText());
	}
	
	private void alignerMultiple() {
		int go = intField(gapOpenField);
		int ge = intField(gapExtensionField);
		ScoreGap score = new ScoreSimpleGap(go, ge);
		if (sequences.get(0) instanceof Proteine) {
			score = new ScoreMatricielGap(Blosum62.matrix, Blosum62.alphabet, go, ge);
		}
		ScoreGapMultiple scoreMultiple = new ScoreGapMultipleSP(score);
		Alignement align = new AlignementAvecGapAffine(sequences, scoreMultiple, score);
		

		textArea.append("\ngo: "+go+"\n");
		textArea.append("go: "+ge+"\n");
		for (int i = 0; i < sequences.getNombre(); i++) {
			textArea.append(align.get(i).getId()+": "+align.get(i).getSequence()+"\n");
		}
		textArea.append("\n");
		textArea.setCaretPosition(textArea.getText().length()-1);
	}
	
	private void aligner(boolean local) {
		try {
			String id1 = seq1Field.getText().trim();
			String id2 = seq2Field.getText().trim();
			int go = intField(gapOpenField);
			int ge = intField(gapExtensionField);
			Sequence seq1 = sequences.get(id1);
			Sequence seq2 = sequences.get(id2);
			ScoreGap score = new ScoreSimpleGap(go, ge);
			if (seq1 instanceof Proteine) {
				score = new ScoreMatricielGap(Blosum62.matrix, Blosum62.alphabet, go, ge);
			}
			Alignement align = null;
			if (local)
				align = new AlignementLocalAvecGapAffine(seq1, seq2, score);
			else
				align = new AlignementAvecGapAffine(seq1, seq2, score);
			textArea.append("\ngo: "+go+"\n");
			textArea.append("go: "+ge+"\n");
			textArea.append(align.get(0).getId()+": "+align.get(0).getSequence()+"\n");
			textArea.append(align.get(1).getId()+": "+align.get(1).getSequence()+"\n");
			textArea.append("\n");
			textArea.setCaretPosition(textArea.getText().length()-1);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Error while aligning sequences!\n");
		}
	}
	
	public static void main(String[] args) {
		new FenetreSequence();
	}
	
}
